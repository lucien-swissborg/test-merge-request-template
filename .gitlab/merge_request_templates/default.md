# Merge request template

## For merger

**1. What this MR does / why we need it:**

-
-

**2. Make sure that you've checked the boxes below before you submit MR:**

- [ ] I have documented my code.
- [ ] I have tested my code.
- [ ] I followed the styling guidelines.
- [ ] no conflict with master branch.

**3. Which issues this PR fixes (optional)**

*Link to jira issues*

- 
- 

**4. CHANGELOG/Release Notes (optional)**


## For reviewers

- [ ] The context for the MR given is clear.
- [ ] Code follows formatting guidelines.
- [ ] Code is clear and well documented.
- [ ] Issues mentioned are clearly addressed and all code added serves a purpose.
- [ ] All discussions/questions worth addressing have been solved in the comments or added commits.
- [ ] There are tests for added components or to prevent fixed issues from happening again.


